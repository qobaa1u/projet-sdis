package telecomnancy;

import telecomnancy.MessageInQueue;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.BASE64Encoder;


public class WorkerManager {


 // Qos qui assure un niveau de qualité de service au client 

    private static ObjectMapper mapper = new ObjectMapper();
    private static ArrayList<Worker> workers = new ArrayList<>();
    private static ArrayList<Worker> workersDoneList = new ArrayList<>();
    private final static int DEFAULT_WAIT_TIME = 10000;
    private final static int BIG_SIZE_QUEUE = 5;



    public static void main(String[] args) throws IOException, Exception {

        int idealNumberOfWorkers;

        while (true) {
            idealNumberOfWorkers = 1;
            ArrayList<Ordre> ordersInQueue = fetchOrdersInQueue();

            if (ordersInQueue.size() > BIG_SIZE_QUEUE) {
                idealNumberOfWorkers += (int) ordersInQueue.size() / BIG_SIZE_QUEUE;
            }

            for (Worker worker : workers) {
                if (worker.isDone()) {
                    workersDoneList.add(worker);
                }
            }

            // avoiding adding workers randomly 
            idealNumberOfWorkers = idealNumberOfWorkers - workers.size();

            if ((idealNumberOfWorkers > 0 && !workers.isEmpty()) || workers.size() < 1) {
                
                for (int i = 0; i < idealNumberOfWorkers; i++) {
                    Worker worker = new Worker();
                    workers.add(worker);
                    worker.start();
                }
            } else if (idealNumberOfWorkers < 0 && workers.size() > 1 && !workersDoneList.isEmpty()) {
               
                for (int i = 0; i < Math.abs(idealNumberOfWorkers); i++) {
                    Worker worker = workersDoneList.get(0);
                    worker.interrupt();
                    workers.remove(worker);
                    workersDoneList.remove(worker);
                }
            }
            System.out.println("Seuil: " + BIG_SIZE_QUEUE + ", Queue size: " + ordersInQueue.size() + ", Workers Number: " + workers.size());
            Thread.sleep(DEFAULT_WAIT_TIME);
        }
    }

  //http request to the web management api of rabbit MQ 
    public static ArrayList<Ordre> fetchOrdersInQueue() {
        ArrayList<Ordre> res = new ArrayList<>();

        try {
            URL url = new URL("http://" + Worker.SERVER_HOST + ":55672/api/queues/%2F/" + Worker.TASK_QUEUE_NAME + "/get");
            String params = "{\"count\":1000,\"requeue\":true,\"encoding\":\"auto\"}";
            byte[] postDataBytes = params.getBytes("UTF-8");

            String userPassword = "guest:guest";
            String userEncoding = new BASE64Encoder().encode(userPassword.getBytes());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setRequestProperty("Authorization", "Basic " + userEncoding);
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);

            InputStream is;
            if (conn.getResponseCode() >= 400) {
                is = conn.getErrorStream();
            } else {
                is = conn.getInputStream();
            }
            Reader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String rawMessagesInQueue = "";
            int c;
            while ((c = in.read()) >= 0) {
                rawMessagesInQueue += (char) c;
            }
            System.err.println(rawMessagesInQueue);

            ArrayList<MessageInQueue> messagesInQueue = mapper.readValue(rawMessagesInQueue, new TypeReference<ArrayList<MessageInQueue>>() {
            });

            for (MessageInQueue message : messagesInQueue) {
                Ordre order = mapper.readValue(message.getPayload(), Ordre.class);
                res.add(order);
            }

            in.close();
        } catch (MalformedURLException ex) {
            Logger.getLogger(WorkerManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WorkerManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }
}
