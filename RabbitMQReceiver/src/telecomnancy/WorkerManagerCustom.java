package telecomnancy;

import telecomnancy.MessageInQueue;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.BASE64Encoder;


public class WorkerManagerCustom {


// Add or remove worker relying on the total charge of the queue. 
    private static ObjectMapper mapper = new ObjectMapper();
    private static ArrayList<Worker> workers = new ArrayList<>();
    private static ArrayList<Worker> workersDoneList = new ArrayList<>();
    private final static int DEFAULT_WAIT_TIME = 10000;
    private final static int BIG_TIME_LOAD = 5000; // limit 
  // comparing the totale charge of the queue, if total_charge > limit then add worker
   // Cf énoncé 
    public static void main(String[] args) throws IOException, Exception {

        int idealNumberOfWorkers;
        int waitTime, waitTimeTmp;
        int chargeTotale;

        while (true) {
            idealNumberOfWorkers = 1;
            waitTime = DEFAULT_WAIT_TIME;
            waitTimeTmp = 10000000;
            chargeTotale = 0;
            ArrayList<Ordre> ordersInQueue = fetchOrdersInQueue();

            for (Ordre ordre : ordersInQueue) {
               
                chargeTotale += ordre.getCharge();
          
                if (ordre.getCharge() > BIG_TIME_LOAD && ordre.getCharge() < waitTimeTmp) {
                    waitTime = ordre.getCharge();
                    waitTimeTmp = ordre.getCharge();                   
                }
            }

            if (chargeTotale > BIG_TIME_LOAD) {
                idealNumberOfWorkers += (int) chargeTotale / BIG_TIME_LOAD;
            }

            if (idealNumberOfWorkers > ordersInQueue.size() && idealNumberOfWorkers > 1) {
                idealNumberOfWorkers = ordersInQueue.size();
            }

            for (Worker worker : workers) {
                if (worker.isDone()) {
                    workersDoneList.add(worker);
                }
            }

            idealNumberOfWorkers = idealNumberOfWorkers - workers.size();

    
            if ((idealNumberOfWorkers > 0 && !workers.isEmpty()) || workers.size() < 1) {
         
                for (int i = 0; i < idealNumberOfWorkers; i++) {
                    Worker worker = new Worker();
                    workers.add(worker);
                    worker.start();
                }
            } else if (idealNumberOfWorkers < 0 && workers.size() > 1 && !workersDoneList.isEmpty()) {
               
                for (int i = 0; i < Math.abs(idealNumberOfWorkers); i++) {
                    Worker worker = workersDoneList.get(0);
                    worker.interrupt();
                    workers.remove(worker);
                    workersDoneList.remove(worker);
                }
            }

            System.out.println("Seuil: " + BIG_TIME_LOAD + ", Queue size: " + ordersInQueue.size() + ", Workers Number: " + workers.size());
            Thread.sleep(waitTime + 500);
        }
    }

   // HTTP request to the web Management API of rabbitMQ 
    public static ArrayList<Ordre> fetchOrdersInQueue() {
        ArrayList<Ordre> res = new ArrayList<>();

        try {
            URL url = new URL("http://" + Worker.SERVER_HOST + ":55672/api/queues/%2F/" + Worker.TASK_QUEUE_NAME + "/get");
            String params = "{\"count\":1000,\"requeue\":true,\"encoding\":\"auto\"}";
            byte[] postDataBytes = params.getBytes("UTF-8");

            String userPassword = "guest:guest";
            String userEncoding = new BASE64Encoder().encode(userPassword.getBytes());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setRequestProperty("Authorization", "Basic " + userEncoding);
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);

            InputStream is;
            if (conn.getResponseCode() >= 400) {
                is = conn.getErrorStream();
            } else {
                is = conn.getInputStream();
            }
            Reader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String rawMessagesInQueue = "";
            int c;
            while ((c = in.read()) >= 0) {
                rawMessagesInQueue += (char) c;
            }
            System.err.println(rawMessagesInQueue);
            ArrayList<MessageInQueue> messagesInQueue = mapper.readValue(rawMessagesInQueue, new TypeReference<ArrayList<MessageInQueue>>() {
            });

            for (MessageInQueue message : messagesInQueue) {
                Ordre order = mapper.readValue(message.getPayload(), Ordre.class);
                res.add(order);
            }

            in.close();
        } catch (MalformedURLException ex) {
            Logger.getLogger(WorkerManagerCustom.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WorkerManagerCustom.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }
}
