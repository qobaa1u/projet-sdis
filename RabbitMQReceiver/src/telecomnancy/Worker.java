package telecomnancy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Worker extends Thread {

    public static final String SERVER_HOST = "ec2-54-157-2-56.compute-1.amazonaws.com"; 
    public static final int SERVER_PORT = 5672;
    public static final String TASK_QUEUE_NAME = "QOBAA_SALVESTRINI_task_queue";
    private Channel channel;
    private static ObjectMapper mapper = new ObjectMapper();
    private boolean done = false;
 
// Establish connexion
    public QueueingConsumer initWorker() throws IOException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(SERVER_HOST);
        factory.setPort(SERVER_PORT);
        Connection connection = factory.newConnection();

        channel = connection.createChannel();
        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
        System.out.println(" [==========] Waiting for messages.  PRESS CTRL+C to kill the process [========]");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(TASK_QUEUE_NAME, false, consumer);

        return consumer;
    }

		//passive waiting with java sleep function
    private void doWork(int n) throws InterruptedException {
        Thread.sleep(n);
    }

    @Override
    public void run() {

        QueueingConsumer consumer = null;
        try {
            consumer = initWorker();
        } catch (IOException ex) {
            Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
            try {
                channel.close();
            } catch (IOException ex1) {
                Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex1);
            }
            this.interrupt();
            return;
        }
        while (true) {
            QueueingConsumer.Delivery delivery = null;
            try {
                delivery = consumer.nextDelivery();
                done = false;
                
                String ordreAsString = new String(delivery.getBody());
                Ordre ordre = mapper.readValue(ordreAsString, Ordre.class);
                System.out.println(" [==========] Received '" + ordre.getMessage() + "'"+"[=============]");
                doWork(ordre.getCharge());

                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                done = true;
                System.out.println("[==============] Done [====================]");

            } catch (InterruptedException ie) {
           
                try {
                    if (delivery != null) {
                        channel.basicReject(delivery.getEnvelope().getDeliveryTag(), true);
                    }
                    channel.close();
                } catch (IOException ex) {
                    Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
                }

                Thread.currentThread().interrupt();
                return;
            } catch (Exception e) {
                System.err.println(e);
                try {
                    channel.close();
                } catch (IOException ex) {
                    Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
                }
                return;
            }
        }

    }
  
    public boolean isDone() {
        return done;
    }
    
   // to purge the queue ... 
    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(SERVER_HOST);
        factory.setPort(SERVER_PORT);
        Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();
        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
        channel.queuePurge(TASK_QUEUE_NAME);
        channel.close();
        System.exit(0);
    }
}
