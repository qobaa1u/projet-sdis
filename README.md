/********************************************************************************************************************/

Travail réalisé par Saad QOBAA & Lolita SALVESTRINI dans le cadre du module de SDIS pour l'année scolaire 2015-2016.

/*********************************************************************************************************************/


1. Déployez le ClientWebServiceApplication sous un serveur GlassFish

2. Accéder à l'interface web en suivant l'url : http://localhost:8080/ClientWS/ClientWS?Tester  (Cf Note en bas) 


3. Le formulaire permet de lancer n fois le clientWebService pour ajouter n messages. 
  3.1  Le premier argument correspond à l'identificateur du message :  Le titre. 
  3.2  Le second argument correspond à la durée de traîtement en millisecondes : sa charge. 


4. Lancez le WorkerManager pour effectuer le traîtement des messages dans la queue. 

Notes : --Ce traitement se fait par rapport à la taille de la queue. Si cette taille dépasse le seuil fixé, on ajoute le nombre nécessaire de workers et dès qu'elle repasse en dessous du seuil, on supprime les workers superflus. 
	-- Un WorkerManagerCustom est disponible afin de traîter les messages présents dans la queue en fonction de la charge de temps totale de la queue. 
	-- Purge de la queue distante : Main de la classe Worker. 
	-- Les messages sont des objets de la classe Ordre, nous utilisons la bibliothèque Jackson afin de sérialisé les messages en JSON dans la queue. 
	-- Nous utilisons l'API HTTP du plugin management de RabbitMQ. Il s'agit de la méthode fetchOrdersInQueue() où on récupère les statistiques sur la queue et nous contrôlons le nombre de workers a alloué en fonction de cela.

	-- L'adresse d'écoute peut dans certains cas être différentes de localhost : http://nom_du_pc:8080/ClientWS/ClientWS?Tester.




