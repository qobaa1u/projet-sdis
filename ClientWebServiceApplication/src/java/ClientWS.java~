import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;

/**
 * @author Saad
 */
@WebService(serviceName = "ClientWS")
@Stateless()
public class ClientWS {

    public static final String SERVER_HOST = "localhost"; // "ec2-54-73-194-42.eu-west-1.compute.amazonaws.com"
    public static final int SERVER_PORT = 5672;
    private static final String TASK_QUEUE_NAME = "qobaa_task_queue";
    private static ObjectMapper mapper = new ObjectMapper();

    /**
     * Web service operation
     */
    @WebMethod(operationName = "sendOrderToQueue")
    public String sendOrderToQueue(@WebParam(name = "message") String message, @WebParam(name = "charge") String charge) throws IOException, ClassNotFoundException {
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        int chargeInt;
        
        //on vérifie que le 2e champ correspondant à la charge est bien un entier
        try{
            chargeInt = Integer.parseInt(charge);
        }catch(NumberFormatException e){
            return "Entier onligatoire dans le second champ";
        }
        
        if(message == null)
            return "Le cahmps message doit être rempli !";
        
        else if(chargeInt <= 0 )
            return "Charge impérativement positive";
        
        else {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(SERVER_HOST);
            factory.setPort(SERVER_PORT);
            Connection connection = factory.newConnection();

            Ordre order = new Ordre();
            order.setMessage(message);
            order.setCharge(chargeInt);
            String orderString = mapper.writeValueAsString(order);

            Channel channel = connection.createChannel();
            channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
            channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, orderString.getBytes("UTF-8"));
            System.out.println(" [=========] Sent '" + order.getMessage() + "'"+"[====================]");

            channel.close();
            connection.close();

            return orderString;
        }
    }
}
